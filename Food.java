/**
 * @(#) Food.java
 */

package RestaurantOwner;

public abstract class Food
{
	private int id;
	
	private String name;
	
	private String quality;
	
	private float price;
	
	private float cost;
	

	private java.util.List<Ingredients> ingredients;
	
	public abstract void prepare( );
	
	public int computeCost( )
	{
		return 0;
	}	
	
	public String getName( )
	{
		return name;
	}
	
	
	public void setPrice( float price )
	{
		this.price=price;
	}
	
	
	public float getPrice( )
	{
		return price;
	}
	
	
	
	
	public String getQuality( )
	{
		return quality;
	}
	
	

	public void setName( String name )
	{
		this.name=name;
	}
	
	
	public void setQuality( String quality )
	{
		this.quality=quality;
	}
	
	public void buyIngredient(Float cost){
		this.ingredients.add(new Ingredients(cost));
		
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}
}
