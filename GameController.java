/**
 * @(#) GameController.java
 */

package RestaurantOwner;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class GameController {
	private int timer = 0;
	private Player player = null;

	private java.util.List<Client> clients;

	public static void main(String[] args) {

		GameController me = new GameController();
		Scanner scanner = new Scanner(System.in);
		System.out
				.println("type Y or y to start to skip inputs, I will fill the your name and your restaurant name for you");
		String initial = scanner.nextLine();
		String name = "Mohamad";
		String surname = "Qaddura";
		String rAddress = "Tartu, Raatuse 22";
		String rCity = "Tartu";
		String rName = "607";
		if (initial.toUpperCase().equals("Y")) {
			// take values above
		} else {
			System.out.println("Hello Gamer to the Game!");
			System.out.println("Please Write your name");
			name = scanner.nextLine();
			System.out.println("Please Write your surname");
			surname = scanner.nextLine();

			System.out.println("Choose the address of the Restaurant");
			rAddress = scanner.nextLine();

			System.out.println("Choose the name of the Restaurant");
			rName = scanner.nextLine();

			System.out.println("Choose the city of the Restaurant");
			rCity = scanner.nextLine();

		}
		System.out.println("Getting everything ready for you please wait!");
		me.initialize(name, surname, rAddress, rName, rCity);
	}

	public void checkCurrentTime() {
		/*
		 * 
		 * if timer % 24 hours is 0 Call dailyPrompt else if timer % 1 week is 0
		 * Call weeklyPrompt else if timer % 1 month is 0 Call montlyPrompt end
		 * if
		 */
	}

	public void dailyPrompt() {
		/*
		 * While not all waiters are assigned to tables get an available waiter
		 * print free tables save input into table Call AssignTable(table) in
		 * Waiter End While
		 */
	}

	public void weeklyPrompt() {
		/*
		 * foreach worker in workers Call Restaurant.PayWorker(worker) if
		 * Restaurant Budget < 0 Call gameOver end if
		 */

		/*
		 * declare amount to pay from buyed ingredients Call
		 * Restaurant.paySupplier(amount) if Restaurant Budget < 0 Call gameOver
		 * end if
		 */
	}

	public void monthlyPrompt() {
		/*
		 * reduce from Restaurant budget if Restaurant Budget < 0 Call gameOver
		 * else Call gameWin
		 */
	}

	public void logInPrompt() {

		/*
		 * do print "what is your name" save input into name while name is used
		 * or empty create a player with the name
		 */
	}

	public void gameOver() {
		/*
		 * print "you lose" print timer for time played print logs
		 */
	}

	public void gameWin() {
		/*
		 * print "you won" print logs Call RankingList.compareScore(budget)
		 */

	}

	public void setTimer(int timer) {
		this.timer = timer;
	}

	public int getTimer() {
		return timer;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Player getPlayer() {
		return player;
	}

	public void initialize(String name, String surName, String rAddress,
			String rName, String rCity) {

		player = new Player(name, surName);
		player.setRestaurant(new Restaurant(rAddress, rName, rCity));
		System.out.println("you have your own restaurant now");
		System.out.println("time to create some Beverages");
		player.createFood();
		player.distributeWaiters();
		resetGameController();
		for (int i = 0; i < 18; i++) {
			clients.add(new Client("Client", "One", 123123 + i, "Tax" + i));
		}
		startGame();
	}

	public void resetGameController() {
		this.clients = new ArrayList<Client>();

	}

	public void startGame() {
		System.out.print("Game Started");
		System.out
				.print("we invited clients to " + inviteClients() + " tables");

	}

	public int inviteClients() {
		Random gen = new Random();

		int allowed = 2;
		switch (player.getRestaurant().getReputation()) {
		case "MEDIUM":
			allowed = 5;
			break;
		case "HIGH":
			allowed = 10;
		default:
			break;
		}

		int tableRandom = gen.nextInt(9);
		int clientRandom = gen.nextInt(18);
		int foodRandom = gen.nextInt(5);
		int dishRandom = gen.nextInt(5);
		for (int i = 0; i < allowed; i++) {
			int cr1 = (clientRandom + i) % 18;
			int cr2 = (clientRandom + i + 1) % 18;
			int tr = (tableRandom + i) % 9;
			int br1 = (foodRandom + i) % 5;
			int br2 = (foodRandom + i + 1) % 5;
			int dr1 = ((foodRandom + i) % 5) + 5;
			int dr2 = ((foodRandom + i) % 5) + 5;
			System.out.print("Client" + clients.get(cr1) + "is ordering");
			clients.get(cr1).placeOrder(
					player.getRestaurant().getTables().get(tr),
					(Beverage) player.getMenu().getFood(dr1),
					(Dish) player.getMenu().getFood(dr1));
			clients.get(cr2).placeOrder(
					player.getRestaurant().getTables().get(tr),
					(Beverage) player.getMenu().getFood(dr2),
					(Dish) player.getMenu().getFood(dr2));

		}

		return allowed;
	}

	public void initialize(String name, String surName) {

	}

	public void decideFoodQuality() {

	}

}
