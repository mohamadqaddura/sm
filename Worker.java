/**
 * @(#) Worker.java
 */

package RestaurantOwner;

public abstract class Worker extends Person
{
	private int name;
	
	private int surname;
	
	private int id;
	
	private float salary;
	
	private String experience = "LOW";
	
	
	
public abstract float calculateSalary( );
	
	public abstract void startTraining( float cos );
	
	public abstract float requestWeeklySalary( float cost );
	
	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}
	
	
}
