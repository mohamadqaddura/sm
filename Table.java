/**
 * @(#) Table.java
 */

package RestaurantOwner;

public class Table {
	private int Id;

	private final int capacity = 2;

	private int seated = 0;

	private int tableNumber;

	private String service = "LOW";

	private boolean served = false;

	public Table(int tableNumber, String experience) {
		this.setTableNumber(tableNumber);
		this.setService(experience);
	}

	public void checkAvailability() {

	}

	public void assignWaiter(Waiter waiter) {

	}

	public void setSeated(int seated) {
		this.seated = seated;
	}

	public int getSeated() {
		return seated;
	}

	public void setTableNumber(int tableNumber) {
		this.tableNumber = tableNumber;
	}

	public int getTableNumber() {
		return tableNumber;
	}

	public void setAvailable(boolean available) {
		served = available;
	}

	public boolean isAvailable() {
		return served == false;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

}
