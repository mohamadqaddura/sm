/**
 * @(#) Order.java
 */

package RestaurantOwner;

public class Order
{
	private int id;
	
	private String clientTaxCode;
	
	private int calory;
	
	private int volume;
	
	private float price;
	
	private Table table;
	
	private Client client;
	
	private int satisfacetion;
	
	public Order(Client client,Table table, Beverage beverage,Dish dish){
		this.client=client;
		this.table=table;
		this.volume= beverage.getVolume();
		this.calory=dish.getCaloryCount();
		//this.setCost(this.calculateCost(table,beverage,dish));
		
	}
	
	public int getCalryByClient( Client client )
	{
		return 0;
	}
	
	public int getVolumeByClient( Client client )
	{
		return 0;
	}
	
	public boolean getCostByClient( Client client )
	{
		return false;
	}
	
	public Client getClientStatistics( Client client )
	{
		return null;
	}
	
	public float calculateCost(Table table,Beverage beverage,Dish dish){		
		float cost = 0;
		if(beverage.getQuality().equals("LOW")){
			cost+=30F;
			}
		else{ 
			cost +=20F;      
			}
		return cost;
		
	}
	public float calculateSatissfaction(){
		
		return 0;
		
	}

//	public float getCost() {
//		return cost;
//	}
//
//	public void setCost(float cost) {
//		this.cost = cost;
//	}
//	
}
