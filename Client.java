/**
 * @(#) Client.java
 */

package RestaurantOwner;

public class Client extends Person
{
	private int id;
	
	private int telephone;
	
	private String taxCode;
	
	
	private boolean available = true;
	
	
	private java.util.List<Order> orders;
	
	public Client(String name,String surname,int telephone,String taxCode)
	{
		this.setName(name);
		this.setSurname(surname);
		this.telephone=telephone;
		this.taxCode=taxCode;
	}
	
	
	

	
	public Order placeOrder(Table table, Beverage beverage, Dish dish )
	{
		this.orders.add(new Order(this,table,beverage,dish));
		return this.orders.get(this.orders.size()-1);
	}
	
	public void isSatissfiedByService( )
	{
	 
		
	}
	
	public void isSatissfiedByFood( Food food )
	{
		
	}
	
	public void pay( float amount )
	{
		
	}
	
	public void sitOnTable( Table table )
	{
		
	}





	public boolean isAvailable() {
		return available;
	}





	public void setAvailable(boolean available) {
		this.available = available;
	}

	
}
