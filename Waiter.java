/**
 * @(#) Waiter.java
 */

package RestaurantOwner;

import java.util.ArrayList;

public class Waiter extends Worker {

	private boolean available = true;
	private java.util.List<Table> tables;

	public Waiter(String name, String surName) {

		this.setName(name);
		this.setSurname(surName);
		this.setExperience("LOW");
		this.setSalary(this.calculateSalary());
		this.tables = new ArrayList<Table>();

	}

	public void resetWaiter() {
		this.tables = new ArrayList<Table>();
	}

	public float calculateSalary() {
		String exp = this.getExperience();
		float salary = 200;
		switch (exp) {
		case "LOW":
			break;
		case "MEDIUM":
			salary = 300;
			break;
		case "HIGH":
			salary = 400;
		}
		return salary;
	}

	public void startTraining(float cost) {

	}

	public void checkAvailabilityAndMax() {

	}

	public Dish getDish() {
		return null;
	}

	public void getDrink() {

	}

	public float requestWeeklySalary(float cost) {
		return 0;
	}

	public void saveOrder(Food food) {

	}

	public boolean getAvailable() {
		return this.available == true;
	}

	public boolean serveTable(Table table) {

		this.tables.add(table);
		if (this.tables.size() == 3)
			this.available = false;
		return this.getAvailable();
	}

}
