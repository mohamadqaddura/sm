/**
 * @(#) Beverage.java
 */

package RestaurantOwner;

public class Beverage extends Food
{
	private int volume;
	
	private Barman barman;
	
	private Order order;
	
	private Menu menu;
	
	public Beverage(String name,String quality,Float price,int volume){
	this.setName(name);
	this.setPrice(price);
	this.setQuality(quality);
	this.setVolume(volume);
		
	}
	
	public void prepare( )
	{
		
	}
	
	public int getVolume( )
	{
		return 0;
	}
	
	

	public void setVolume( int volume )
	{
		this.volume=volume;
	}
	
	
}
