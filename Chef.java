/**
 * @(#) Chef.java
 */

package RestaurantOwner;

public class Chef extends Worker
{
	private String taxCode;
	
	public Chef(String name,String surName,String taxCode){
		  this.setName(name);
		  this.setSurname(surName);	  
		  this.setExperience("LOW");	
		  this.setSalary(this.calculateSalary());
		  this.setTaxCode(taxCode);
	}
	
	public float calculateSalary()
	{
		String exp = this.getExperience();
		float salary = 300;
		switch(exp){
		case "LOW":
			break;
		case "MEDIUM":
			salary = 400;
			break;
		case "HIGH":
			salary =500;		    
		}		
		return salary;
	}
	
	public void startTraining( float cost )
	{
		
	}
	
	public void prepareFood( Food food )
	{
		
	}
	
	public float RequestWeeklySalary( float cost )
	{
		return 0;
	}

	@Override
	public float requestWeeklySalary(float cost) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}
	
	
}
