/**
 * @(#) Player.java
 */

package RestaurantOwner;

import java.util.Scanner;
import java.util.ArrayList;

public class Player extends Person {

	Scanner scan = new Scanner(System.in);
	private float score;

	public Restaurant restaurant;

	private Menu menu;

	public Player(String name, String surname) {
		this.setName(name);
		this.setSurname(surname);
		this.setMenu(new Menu());
	}

	public Menu getMenu() {
		return this.menu;

	}

	public void setMenu(Menu menu) {
		this.menu = menu;

	}

	public void resetPlayer() {

	}

	public void priceLowQualityBeverage() {

	}

	public void decideDishQuality() {

	}

	public void decidBeverageQuality() {

	}

	public void priceLowQualityDish() {

	}

	public void trainWorker(Worker worker) {

	}

	public void startGame() {

	}

	public void decideFoodCost(Food food, float price) {

	}

	public void payToSupplierEndOfWeek(float amount) {

	}

	public float computeMonthlyScore() {
		return 0;
	}

	public void payWeeklySalary(Worker worker, float amount) {

	}

	public void decideFoodQuality() {
		for (int i = 0; i < 5; i++) {
			// System.out.print("")
		}
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public void ownRestaurant() {

	}

	public boolean decideFoodQuality(Food food, String quality) {
		return false;
	}

	public void createFood() {

		menu.resetMenu();
		System.out
				.println("type Y or y to skip inputs, I will create menu for you");
		String choice = scan.nextLine();
		if (choice.toUpperCase().equals("Y")) {

		} else {

			menu.addFood(new Beverage("B1", "LOW", 1F, 1));
			menu.addFood(new Beverage("B2", "LOW", 2F, 2));
			menu.addFood(new Beverage("B3", "LOW", 3F, 3));
			menu.addFood(new Beverage("B4", "HIGH", 4F, 4));
			menu.addFood(new Beverage("B1", "HIGH", 5F, 5));
			menu.addFood(new Dish("D1", "LOW", 1F, 1));
			menu.addFood(new Dish("D2", "LOW", 2F, 2));
			menu.addFood(new Dish("D3", "LOW", 3F, 3));
			menu.addFood(new Dish("D4", "HIGH", 4F, 4));
			menu.addFood(new Dish("D5", "HIGH", 5F, 5));
            

			System.out.println("Time Adding Beverages");
			for (int i = 0; i < 5; i++) {
				System.out.println("What is the name of the Beverage?");
				String name = scan.nextLine();
				System.out
						.println("LOW or HIGH What is the quality of the Beverage?");
				String quality = scan.nextLine();
				System.out.println("What is the price of the Beverage");
				float price = Float.parseFloat(scan.nextLine());
				System.out.println("What is the volume of the Beverage");
				int volume = Integer.parseInt(scan.nextLine());
				menu.addFood(new Beverage(name, quality.toUpperCase(), price,
						volume));
			}

			System.out.println("Time Adding Dishes");
			for (int i = 0; i < 5; i++) {
				System.out.println("What is the name of the Dish?");
				String name = scan.nextLine();
				System.out.println("What is the quality of the Dish?");
				String quality = scan.nextLine();
				System.out.println("What is the price of the Dish");
				float price = Float.parseFloat(scan.nextLine());
				System.out.println("What is the caloryCount of the Dish");
				int caloryCount = Integer.parseInt(scan.nextLine());
				menu.addFood(new Dish(name, quality, price, caloryCount));
			}

		}

	}

	public void distributeWaiters() {

		System.out
				.println("type Y or y to skip inputs, I will assign waiters to tables for you");
		String choice = scan.nextLine();
		if (choice.toUpperCase().equals("Y")) {
			ArrayList<Table> tables = (ArrayList<Table>) restaurant.getTables();
			for (int i = 0; i < 3; i++) {
				Waiter waiter = (Waiter) restaurant.getWorkers().get(i);
				waiter.serveTable(tables.get(i * 3 + 0));
				restaurant.getTables().get(i * 3 + 0)
						.setService(waiter.getExperience());
				waiter.serveTable(tables.get(i * 3 + 1));
				restaurant.getTables().get(i * 3 + 1)
						.setService(waiter.getExperience());
				waiter.serveTable(tables.get(i * 3 + 2));
				restaurant.getTables().get(i * 3 + 2)
						.setService(waiter.getExperience());
			}

		} else {
			for (int i = 0; i < 3; i++) {
				Waiter waiter = (Waiter) restaurant.getWorkers().get(i);
				Table table = null;
				do {

					ArrayList<Table> tables = (ArrayList<Table>) restaurant
							.getTables();
					for (int j = 0; j < tables.size(); j++) {
						table = tables.get(j);
						if (tables.get(j).isAvailable()) {
							System.out.println("Table "
									+ table.getTableNumber()
									+ "is free, assign to waiter "
									+ waiter.getSurname());
							System.out.println("Y for yes N for No");
							String response = scan.nextLine();
							if (response.equals("Y")) {
								break;
							}
						}

						if (j == 9 && waiter.getAvailable()) {
							System.out.println("this waiter needs more tables");
							j = 0;
						}

					}

				} while (waiter.serveTable(table));
			}

		}
	}

}
