/**
 * @(#) Dish.java
 */

package RestaurantOwner;

public class Dish extends Food
{
	private int caloryCount;
	
	private Chef chef;
	
	private Order order;
	
	private Menu menu;
	
	public Dish(String name,String quality,float price,int caloryCount){
		this.setCaloryCount(caloryCount);
		this.setName(name);
		this.setQuality(quality);
		this.setPrice(price);
		
	}
	
	public void prepare( )
	{
		
	}
	
	public int getCalory( )
	{
		return 0;
	}
	
	

	public void setCaloryCount( int caloryCount )
	{
		this.caloryCount=caloryCount;
	}
	
	
	public int getCaloryCount( )
	{
		return caloryCount;
	}
	
	
}
