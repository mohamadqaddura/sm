/**
 * @(#) Restaurant.java
 */

package RestaurantOwner;

import java.util.ArrayList;
import java.util.List;

public class Restaurant
{
	private String address;
	
	private String name;
	
	private String city;
	
	private int budget = 10000;
	
	private int reputationPoints = 15;
	
	private String reputation = "MEDIUM";
	
	private Supplier supplier;
	
	private java.util.List<Worker> workers ;
	private java.util.List<Table> tables ;
	
	public Restaurant( String address, String name, String City )
	{
		
	    this.setAddress(address);
	    this.setName(name);
	    this.setCity(city);
	    this.setBudget(this.getBudget());
	    this.setReputation(this.getReputation());
	    this.setReputationPoints(this.getReputationPoints());
	   
		this.workers = new java.util.ArrayList<Worker>();
	    this.workers.add(new Waiter("Waiter","1"));
	    this.workers.add(new Waiter("Waiter","2"));
	    this.workers.add(new Waiter("Waiter","3"));
	    this.workers.add(new Chef("Chef","1","taxC11##55"));
	    this.workers.add(new Barman("Barman","1"));
	    
	    System.out.print("initializing tables");
		this.tables=new ArrayList<Table>();
	    for (int i = 0; i < 9; i++) {
			this.tables.add(new Table(i+1,"LOW"));
		}
	}
	
	public void resetRestaurant(){
		this.workers = new java.util.ArrayList<Worker>();
		this.tables=new ArrayList<Table>();
		
	}
	
	public float getMontlyBudget( )
	{
		return 0;
	}
	
	public int computeReputation( )
	{
		return 0;
	}
	
	public float computeBudget( )
	{
		return 0;
	}
	
	public void paySupplier( float amount )
	{
		
	}
	
	public int getRank( float score )
	{
		return 0;
	}
	
	public void payWorker( Worker worker )
	{
		
	}
	
	
	public String getAddress( )
	{
		return address;
	}
	
	
	public String getName( )
	{
		return name;
	}
	

	
	public String getCity( )
	{
		return city;
	}
	
	public void setBudget( int budget )
	{
		this.budget=budget;
	}
	
	public int getBudget( )
	{
		return budget;
	}
	
	public void setReputationPoints( int reputationPoints )
	{
		this.reputationPoints=reputationPoints;
	}
	
	public int getReputationPoints( )
	{
		return reputationPoints;
	}
	
	
	public String getReputation( )
	{
		return reputation;
	}
	

	public List<Worker> getWorkers( )
	{
		return workers;
	}
	




	
	public void setWorkers( List<Worker> workers )
	{
		
	}
	
	

	public void setAddress( String address )
	{
		this.address=address;
	}
	
	
	public void setName( String name )
	{
		this.name=name;
	}
	
	
	public void setCity( String city )
	{
		this.city=city;
	}
	
	
	public void setReputation( String reputation )
	{
		this.reputation=reputation;
	}
	
	public java.util.List<Table> getTables(){
		return this.tables;
	}
	
	public void setTables(java.util.List<Table> tables){
		this.tables=tables;
	}


	
	


	
	
}
