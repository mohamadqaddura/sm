/**
 * @(#) Ingredients.java
 */

package RestaurantOwner;

public class Ingredients {
	private String name;

	private int amount;

	private Supplier supplier;

	private Food food;

	private Float cost;

	public Ingredients(float cost) {
		this.cost = cost;
	}

	public void makeFood(Food food) {

	}

	public void updateIngredients() {

	}

}
