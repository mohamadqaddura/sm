/**
 * @(#) Person.java
 */

package RestaurantOwner;

public class Person
{
	private String name;
	
	private String Surname;
	
	private int Id;
	


	public void setName( String name )
	{
		this.name=name;
	}
	
	
	public String getName( )
	{
		return name;
	}
	
	
	public void setSurname( String surname )
	{
		Surname=surname;
	}
	
	
	public String getSurname( )
	{
		return Surname;
	}
	
	
	public void setId( int id )
	{
		Id=id;
	}
	
	
	public int getId( )
	{
		return Id;
	}
	
	
}
